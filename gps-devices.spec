Name:		gps-devices
Version:	0.1.0
Release:	0.1%{?dist}
Summary:	GPS Devices for navigation

Group:		System Environment/Daemons
License:	GPLv2+
URL:		http://fedorapeople.org/gitweb?p=ndim/public_git/gps-devices-package.git
Source0:	gps-devices-information.fdi
Source1:	gps-devices-policy.fdi
Source30:	README
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:	noarch

Requires:	hal
Requires(pre):	shadow-utils


%description
Automatic setup and use of GPS devices (Global Positioning System)
for navigational purposes.


%prep
%setup -c -T
cp %{SOURCE30} .


%build


%install
rm -rf "%{buildroot}"

install -d -m 0755 "%{buildroot}/usr/share/hal/fdi/information/20thirdparty"
install -p -m 0644 %{SOURCE0} "%{buildroot}/usr/share/hal/fdi/information/20thirdparty/10-gps-devices.fdi"

install -d -m 0755 "%{buildroot}/usr/share/hal/fdi/policy/20thirdparty"
install -p -m 0644 %{SOURCE1} "%{buildroot}/usr/share/hal/fdi/policy/20thirdparty/10-gps-devices.fdi"


%pre
getent group gps >/dev/null || groupadd -r gps
exit 0


%post
service haldaemon restart
exit 0


%preun
exit 0


%postun
service haldaemon restart
exit 0


%clean
rm -rf "%{buildroot}"


%files
%defattr(-,root,root,-)
%doc README
%{_datadir}/hal/fdi/information/20thirdparty/10-gps-devices.fdi
%{_datadir}/hal/fdi/policy/20thirdparty/10-gps-devices.fdi


%changelog
* Tue Jul 21 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.1.0-0.1
- Use real URL: tag

* Sat Jul 11 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.1.0-0
- initial package

